import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import Database.Models.ActivityModel;

public class ActivityAdapter extends ArrayAdapter<ActivityModel> {

    Context context;
    int layoutResource;
    List<ActivityModel> todaysActivites;

    public ActivityAdapter(@NonNull Context context, int layoutResource, List<ActivityModel> todaysActivities) {
        super(context, layoutResource, todaysActivities);

        this.context = context;
        this.layoutResource = layoutResource;
        this.todaysActivites = todaysActivities;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }
}
