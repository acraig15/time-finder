package com.aaroncraig.timefinder;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import DAL.DatabaseHelper;

public class ManuallyAddActivity extends AppCompatActivity implements View.OnClickListener {
    private DatabaseHelper db;
    //Specific fields to get data from
    EditText activityDescription;
    EditText activityDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manually_add);

        activityDescription = (EditText) findViewById(R.id.activityDescription);
        activityDuration = (EditText) findViewById(R.id.activityDuration);

        findViewById(R.id.saveActivityButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.saveActivityButton:
                saveActivity();

                break;
        }
    }

    private void saveActivity() {

        //Error checks the description field
        String description = activityDescription.getText().toString().trim();
        if(description.isEmpty()) {
            activityDescription.setError("Description can't be empty");
            activityDescription.requestFocus();
            return;
        }
        //Error checks the duration field
        String tempDuration = activityDuration.getText().toString().trim();
        if(tempDuration.isEmpty()) {
            activityDuration.setError("Must have a value.");
            activityDuration.requestFocus();
            return;
        }

        Integer duration = Integer.parseInt(activityDuration.getText().toString());

        db = new DatabaseHelper(this);
        db.addNewActivity(description, duration, "true");

        startActivity(new Intent(this, MainActivity.class));
    }
}
