package com.aaroncraig.timefinder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import DAL.DatabaseHelper;
import Database.Models.ActivityModel;
import Utils.MyDividerItemDecoration;
import View.ActivityAdapter;

public class ViewToday extends AppCompatActivity {
    private ActivityAdapter aAdapter;
    private List<ActivityModel> activityList = new ArrayList<>();
    private RecyclerView recyclerView;

    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_today);

        //coordinatorLayout = findViewById(R.id)
        recyclerView = findViewById(R.id.list);
        //Possibly a message if no activites yet

        db = new DatabaseHelper(this);

        activityList.addAll(db.getTodaysActivities());

        aAdapter = new ActivityAdapter(this, activityList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager((getApplicationContext()));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(aAdapter);
    }
}