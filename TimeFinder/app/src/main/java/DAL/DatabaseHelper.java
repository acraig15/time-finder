package DAL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;
import Database.Models.ActivityModel;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "TIME_FINDER_DATABASE";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        //Create the Activities table
        db.execSQL(ActivityModel.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + ActivityModel.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    //Adds a new activity to the database
    public long addNewActivity(String activityDescription, int activityDuration, String addedManually) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ActivityModel.COLUMN_ACTIVITY, activityDescription);
        values.put(ActivityModel.COLUMN_DURATION, activityDuration);
        values.put(ActivityModel.COLUMN_WAS_ADDED_MANUALLY, addedManually);
        //Get the unique id value the function returns
        long id = db.insert(ActivityModel.TABLE_NAME, null, values);

        db.close();

        return id;
    }

    //Reading from the DB
    public List<ActivityModel> getTodaysActivities() {
        List<ActivityModel> todaysActivities = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + ActivityModel.TABLE_NAME + "  WHERE TimeAdded >= date('now', '-1 day') ORDER BY + " + ActivityModel.COLUMN_TIME_ADDED + " DESC";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        //Loop through and gather the data
        if(cursor.moveToFirst()) {
            do {
                ActivityModel a = new ActivityModel();
                a.setId(cursor.getInt(cursor.getColumnIndex(ActivityModel.COLUMN_ID)));
                a.setActivity(cursor.getString(cursor.getColumnIndex(ActivityModel.COLUMN_ACTIVITY)));
                a.setDuration(cursor.getInt(cursor.getColumnIndex(ActivityModel.COLUMN_DURATION)));
                a.setTimeAdded(cursor.getString(cursor.getColumnIndex(ActivityModel.COLUMN_TIME_ADDED)));
                a.setWasAddedManually(cursor.getString(cursor.getColumnIndex(ActivityModel.COLUMN_WAS_ADDED_MANUALLY)));

                todaysActivities.add(a);
            } while(cursor.moveToNext());
        }

        db.close();
        return todaysActivities;
    }
    public List<ActivityModel> getAllActivities() {
        List<ActivityModel> allActivities = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + ActivityModel.TABLE_NAME + " ORDER BY + " + ActivityModel.COLUMN_TIME_ADDED + " DESC";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        //Loop through and gather the data
        if(cursor.moveToFirst()) {
            do {
                ActivityModel a = new ActivityModel();
                a.setId(cursor.getInt(cursor.getColumnIndex(ActivityModel.COLUMN_ID)));
                a.setActivity(cursor.getString(cursor.getColumnIndex(ActivityModel.COLUMN_ACTIVITY)));
                a.setDuration(cursor.getInt(cursor.getColumnIndex(ActivityModel.COLUMN_DURATION)));
                a.setTimeAdded(cursor.getString(cursor.getColumnIndex(ActivityModel.COLUMN_TIME_ADDED)));
                a.setWasAddedManually(cursor.getString(cursor.getColumnIndex(ActivityModel.COLUMN_WAS_ADDED_MANUALLY)));

                allActivities.add(a);
            } while(cursor.moveToNext());
        }

        db.close();
        return allActivities;
    }

}