package View;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.view.View;

import com.aaroncraig.timefinder.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import Database.Models.ActivityModel;

/**
 * Created by Aaron Craig on 5/10/2018.
 */

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.MyViewHolder> {

    private Context context;
    private List<ActivityModel> activityList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView addTime;
        public TextView dot;
        public TextView activityDescription;

        public MyViewHolder(View view) {
            super(view);
            addTime = view.findViewById(R.id.addTime);
            dot = view.findViewById(R.id.dot);
            activityDescription = view.findViewById(R.id.activityDescription);
        }
    }

    public ActivityAdapter(Context context, List<ActivityModel> activityList) {
        this.context = context;
        this.activityList = activityList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ActivityModel a = activityList.get(position);

        holder.activityDescription.setText(a.getActivity());
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.addTime.setText(formatDate(a.getTimeAdded()));
    }

    /**
     * Formatting timestamp to `MMM d` format
     * Input: 2018-02-21 00:15:42
     * Output: Feb 21
     */
    private String formatDate(String dateStr) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = fmt.parse(dateStr);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM d");
            return fmtOut.format(date);
        } catch (ParseException e) {

        }

        return "";
    }
    @Override
    public int getItemCount() {
        return activityList.size();
    }
}
