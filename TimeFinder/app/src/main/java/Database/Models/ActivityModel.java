package Database.Models;

public class ActivityModel {
    //Database information when making it/accessing it
    public static final String TABLE_NAME = "Activities";
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_ACTIVITY = "Activity";
    public static final String COLUMN_DURATION = "Duration";
    public static final String COLUMN_TIME_ADDED = "TimeAdded";
    public static final String COLUMN_WAS_ADDED_MANUALLY = "WasAddedManually";

    //Java properties of the database
    private int id;
    private String activity;
    private int duration;
    private String timeAdded;
    private String wasAddedManually;

    //The SQL to execute to create a table
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_ACTIVITY + " TEXT,"
                + COLUMN_DURATION + " INTEGER,"
                + COLUMN_TIME_ADDED + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
                + COLUMN_WAS_ADDED_MANUALLY + " TEXT"
                + ")";

    public ActivityModel() {

    }

    public ActivityModel(int id, String activity, int duration, String timeAdded, String wasAddedManually) {
        this.id = id;
        this.activity = activity;
        this.duration = duration;
        this.timeAdded = timeAdded;
        this.wasAddedManually = wasAddedManually;
    }



    //Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(String timeAdded) {
        this.timeAdded = timeAdded;
    }

    public String getWasAddedManually() {
        return wasAddedManually;
    }

    public void setWasAddedManually(String wasAddedManually) {
        this.wasAddedManually = wasAddedManually;
    }
}