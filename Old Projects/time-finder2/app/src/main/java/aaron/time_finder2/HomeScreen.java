package aaron.time_finder2;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class HomeScreen extends Activity {
    private NotificationHelper notificationHelper;
    private EditText messageMainUse;
    private EditText messageAppUpdates;
    private Button buttonMainUse;
    private Button buttonAppUpdates;
    private Button buttonSetTimer;
    //Notification slider and text
    private SeekBar nBar;
    private TextView nText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        initVars();

        //Change the slider and text
        nText.setText("Notification Time: " + nBar.getProgress() + " Minutes");
        nBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                nText.setText("Notification Time: " + progress);
                Toast.makeText(getApplicationContext(), "Changing seekbar's progress" + nBar.getProgress(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Toast.makeText(getApplicationContext(), "Started teackign seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //nText.setText("Notification Time: " + progress);
                //Toast.makeText(getApplicationContext(), "Stopped tracking", Toast.LENGTH_SHORT).show();
            }
        });

        //Create the notification channel
        notificationHelper = new NotificationHelper(this);

        //Get message
        messageMainUse = findViewById(R.id.mainUseText);
        messageAppUpdates = findViewById(R.id.appUpdateText);

        //Get buttons for notifications
        buttonMainUse = findViewById(R.id.buttonMainUse);
        buttonAppUpdates = findViewById(R.id.buttonAppUpdate);
        buttonSetTimer = findViewById(R.id.setNotifLength);

        //Buttons to send notifications
        buttonMainUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMainUseNotification("Main Use", messageMainUse.getText().toString());
            }
        });

        buttonAppUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendAppUpdatesNotification("App Updates", messageAppUpdates.getText().toString());
            }
        });

        buttonSetTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), nBar.getProgress() * 60 * 1000, Toast.LENGTH_SHORT);
                startAlarm();
            }
        });

    }

    private void startAlarm() {
        AlarmManager am = (AlarmManager) getSystemService((Context.ALARM_SERVICE));
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);

        am.setExact(am.RTC_WAKEUP, /*Time here*/ 1000, pendingIntent);

    }

    //Private function for seekbar
    private void initVars() {
        nBar = (SeekBar) findViewById(R.id.notificationTime);
        nText = (TextView) findViewById(R.id.notificationText);
    }

    public void sendMainUseNotification(String title, String message) {
        Notification.Builder nb = notificationHelper.getMainUseNotification(title, message);
        notificationHelper.notify(1, nb);
    }

    public void sendAppUpdatesNotification(String title, String message) {
        Notification.Builder nb = notificationHelper.getAppUpdatesNotification(title, message);
        notificationHelper.notify(2, nb);
    }
}