package aaron.time_finder2;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.Color;
import android.content.Context;
import android.content.ContextWrapper;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;

class NotificationHelper extends ContextWrapper {
    private NotificationManager notifManager;
    //Channel for using the app
    public static final String CHANNEL_ONE_ID = "mainUse";
    public static final String CHANNEL_ONE_NAME = "Add New Entry";
    //Channel details for updates about the app
    public static final String CHANNEL_TWO_ID = "appUpdates";
    public static final String CHANNEL_TWO_NAME = "Time Finder";

//Create your notification channels//

    public NotificationHelper(Context base) {
        super(base);
        createChannels();
    }

    public void createChannels() {
        //Create the main use channel
        NotificationChannel mainUse = new NotificationChannel(CHANNEL_ONE_ID, CHANNEL_ONE_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        mainUse.enableLights(true);
        mainUse.enableVibration(true);
        mainUse.setLightColor(R.color.colorPrimary);
        mainUse.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        getManager().createNotificationChannel(mainUse);

        //Create the app updates channel
        NotificationChannel appUpdates = new NotificationChannel(CHANNEL_TWO_ID, CHANNEL_TWO_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        appUpdates.enableLights(true);
        appUpdates.enableVibration(true);
        appUpdates.setLightColor(R.color.colorPrimary);
        appUpdates.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        getManager().createNotificationChannel(appUpdates);

        /* Old way
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ONE_ID,
                CHANNEL_ONE_NAME, notifManager.IMPORTANCE_HIGH);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.setShowBadge(true);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(notificationChannel); */

    }

    //Create the notification that’ll be posted to mainUse channel//

    public Notification.Builder getMainUseNotification(String title, String body) {

        //Add buttons to the notification


        return new Notification.Builder(getApplicationContext(), CHANNEL_ONE_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.ic_temp1)
                .setAutoCancel(true);
    }

    public Notification.Builder getAppUpdatesNotification(String title, String body) {

        //Add buttons to the notification


        return new Notification.Builder(getApplicationContext(), CHANNEL_TWO_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.ic_temp2)
                .setAutoCancel(true);
    }

    public void notify(int id, Notification.Builder notification) {
        getManager().notify(id, notification.build());
    }

//Send your notifications to the NotificationManager system service//

    public NotificationManager getManager() {
        if (notifManager == null) {
            notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notifManager;
    }
}