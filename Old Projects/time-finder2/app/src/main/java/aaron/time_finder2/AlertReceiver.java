package aaron.time_finder2;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Aaron Craig on 4/13/2018.
 */

public class AlertReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHelper notificationHelper = new NotificationHelper(context);
        Notification.Builder nb = notificationHelper.getMainUseNotification("Test", "Is it working?");
        notificationHelper.getManager().notify(1, nb.build());
    }
}
