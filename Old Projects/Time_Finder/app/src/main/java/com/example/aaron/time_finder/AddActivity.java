package com.example.aaron.time_finder;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class AddActivity extends AppCompatActivity {

    private NotificationHelper notificationHelper;
    public static final String EXTRA_MESSAGE = "com.example.aaron.time_finder.MESSAGE";
    private EditText messageMainUse;
    private EditText messageAppUpdates;
    private Button buttonMainUse;
    private Button buttonAppUpdates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Create the notification channel
        notificationHelper = new NotificationHelper(this);

        //Get buttons for notifications
        buttonMainUse = findViewById(R.id.buttonChannelMainUse);
        buttonAppUpdates = findViewById(R.id.buttonChannelAppUpdates);

        buttonMainUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMainUseNotification("Main Use", messageMainUse.getText().toString());
            }
        });

        buttonMainUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendAppUpdatesNotification("App Updates", messageAppUpdates.getText().toString());
            }
        });


        /* Old way
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Add a previous activity.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }); */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Old way, something wrong with it
    /*public void sendNotification() {
        Notification.Builder nB = notificationHelper.getNotification("Test", "Is this working?");

        notificationHelper.notify(101, nB);
    } */

    public void sendMainUseNotification(String title, String message) {
        Notification.Builder nb = notificationHelper.getMainUseNotification(title, message);
        notificationHelper.getManager().notify(1, nb.build());
    }
    public void sendAppUpdatesNotification(String title, String message) {
        Notification.Builder nb = notificationHelper.getAppUpdatesNotification(title, message);
        notificationHelper.getManager().notify(2, nb.build());
    }

    /*public void postNotification(int id, String title) {
        Notification.Builder notificationBuilder =
                notificationBuilder = notificationHelper.getNotification1("Test",
                        "Is this working?");

        if (notificationBuilder != null) {
            notificationHelper.notify(id, notificationBuilder);
        }
    }

    public void onClick(View view) {
        postNotification(101, "Test");
    } */

}
