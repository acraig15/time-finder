package com.example.aaron.time_finder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        //get intent that started this one
        Intent intent = getIntent();
        String message = intent.getStringExtra(AddActivity.EXTRA_MESSAGE);

        //Capture layout's textview and set string to its text
        TextView textView = findViewById(R.id.textView);
        textView.setText(message);
    }
}
