﻿## Aaron Craig's Personal Project for Senior Capstone

My [Vision Statement](VisionStatement.md) describes why this app is different, and what I hope to accomplish by creating it. This app is for me, as I've tried many times in the past to track my time, but doing it manually is difficult, and this app should fix that problem.

---

##Time Finder

#Vision Statement

---

For everyone who has ever felt like they don't have enough time in the day, the Time Finder Android app will help you to find where your time is going and reclaim for the things you really care about. Time Finder will be a semi-automatic tracking app to figure out what you do during your day, and where you can find time to work on your personal projects, or go running, or whatever is you want to do. Once or twice an hour, a quick notification will appear on your Android phone asking what you've been doing, and you can input the reply without leaving whatever you're doing. Then, over the course of a day, the app will graphically display how you used your day. Unlike other time tracking apps, this one is easily configurable when it needs input, and doesn't require you to ever open the app to input what you've been doing during the day.